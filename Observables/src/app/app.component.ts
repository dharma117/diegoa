import { Component } from '@angular/core';

import { ObservableService } from './observable.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  isLoggedInHeader: boolean;

  isLoggedIn: boolean;


  constructor(private observableService:ObservableService){

  }
  ngOnInit(){
    this.observableService.isLoggedInObservable.subscribe(isLoggedInSeviceValue=>{
      console.log("isLoggedInObservable",isLoggedInSeviceValue);
      this.isLoggedInHeader = isLoggedInSeviceValue;
    });
  }

  login(){
    this.observableService.login().subscribe(response=>{
      console.log("response",response);
      this.isLoggedIn = response;
    });
  }
}
