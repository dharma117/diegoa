import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable, Observer } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class ObservableService {
  isLoggedInSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  _isLoggedIn: boolean;
  get isLoggedIn(){
    return this._isLoggedIn;
  }
  set isLoggedIn(value: boolean){
    this._isLoggedIn = value;
    this.isLoggedInSubject.next(this._isLoggedIn);
  }


  get isLoggedInObservable(){
    return this.isLoggedInSubject.asObservable();
  }

  constructor() { }

  login(){
    return Observable.create((observer:Observer<any>)=>{
      setTimeout(()=>{
        observer.next({success:true});
        observer.complete();
      },1500);
    }).map(response=>{
      this.isLoggedIn = true;
      return response.success;
    });
  }
}
